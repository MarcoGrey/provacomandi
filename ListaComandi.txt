Lista comandi git e procedure:

1- Fork cartella originale 

2- Modifica vari permessi (lettura/scrittura/admin) e README

3- 'git clone [url]' per copiare la cartella in local

4- 'git flow init' per inizializzare

5- Dare un nome ai vari tipi di branch (consigliato stare su un nome generico
	Es. Feature branches? [] feature // bugfix branches? [] bugfix...

---------------------------------------------------------------------------------------------------------------------

6- 'git flow feature start <name>' crea un nuovo branch dove lavorare
	Es. 'git flow feature start Prova1' crea un nuovo branch featureProva1 all'interno di develop

7- Implementare un test e verificare che il codice compili ma che il test dia risultato negativo (ROSSO)

8- 'git add .' per aggiungere i file allo scopo di fare il commit

9- 'git commit -m "ROSSO"' per effettuare il commit rosso

10- Modificare il codice in modo che il test dia risultato positivo (VERDE)

11- 'git add .' per aggiungere i file allo scopo di fare il commit

12- 'git commit -m "VERDE"' per effettuare il commit verde

13- Se necessario effettuare un refactoring (controllo e miglioramento) del codice e se il test � ancora
	verde ricarica tutto ( 'git add .' // 'git commit -m "REFACTORING")

14- 'git flow feature finish <nome>' per chiudere il branch
	Es. 'git flow feature finish Prova1' esegue il merge del branch per la funzionalit� sviluppata 


SE SI VUOLE FARE UN ALTRO TEST TORNARE AL PUNTO 6 ALTRIMENTI PROSEGUIRE

---------------------------------------------------------------------------------------------------------------------

15- 'git flow release start <nome>' per aprire il branch e caricare il progetto nella release
	Es. 'git flow release start 1.0'

16- 'git flow release finish <nome>' per chiudere il branch
	Es. 'git flow release finish 1.0'

(BASTANO QUESTI DUE COMANDI PER CREARE LA RELEASE)

SE SI VUOLE FARE UN ALTRO TEST TORNARE AL PUNTO 6 ALTRIMENTI PROSEGUIRE

----------------------------------------------------------------------------------------------------------------------

17- 'git push origin --all --follow-tags' per caricare tutto nel branch Master

FINE :) RICORDA DI CONTROLLARE SEMPRE ANCHE SU BITBUCKET PER ESSERE SICURO CHE VADA TUTTO BENE

'git branch' restituisce la lista dei branch

CONSIGLIABILE FARE UNA RELEASE PRIMA DELL'EFFETTIVO COMPLETAMENTO DEL PROGRAMMA (NEI LABORATORI VENIVA SCRITTO QUANDO FARLA)
